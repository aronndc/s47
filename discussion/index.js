// alert("Hello World!");

// querySelector() is a method that can be use to select a specific element from our document.
console.log(document.querySelector("#txt-first-name"));

// document refers to the whole page.
console.log(document);


/*
	Alternative methods that we can use aside from querySelector in retrieving elements.

	Syntax:
		document.getElementById()
		document.getElementByClassName()
		document.getElementByTagName()
*/

// DOM Manipulation

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(spanFullName);


/*
	Event:
		click, hover, keypress, keyup and many others.

	Event listeners:
		Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.

	Syntax:
		selectedElement.addEventListener('event', function);
*/


/*txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
});*/

// Other Way
txtFirstName.addEventListener('keyup', printFirstName);
function printFirstName (event) {spanFullName.innerHTML = txtFirstName.value};


/*
	innerHTML - is a property of an element which considers all the children of the selected element as a string.

	.value of the input text field.
*/


txtFirstName.addEventListener('keyup', (event) =>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});